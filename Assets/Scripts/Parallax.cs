﻿using UnityEngine;
using System.Collections;

public class Parallax : MonoBehaviour {

	public Transform[] backgrounds;			
	private float[] parallaxScales;			
	public float smoothing = 1f;			

	private Transform cam;					
	private Vector3 previousCamPos;			


	void Awake () {
		cam = Camera.main.transform;
	}


	void Start () {
		// The previous frame had the current frame's camera position
		previousCamPos = cam.position;

		parallaxScales = new float[backgrounds.Length];
		for (int i = 0; i < backgrounds.Length; i++) {
			parallaxScales[i] = backgrounds[i].position.z*-1;
		}
	}
	

	void Update () {

		// for each background
		for (int i = 0; i < backgrounds.Length; i++) {

			float parallax = (previousCamPos.y - cam.position.y) * parallaxScales[i];

			float backgroundTargetPosY = backgrounds[i].position.y + parallax;

            Vector3 backgroundTargetPos = new Vector3(backgrounds[i].position.x, backgroundTargetPosY, backgrounds[i].position.z);

            backgrounds[i].position = Vector3.Lerp(backgroundTargetPos, backgrounds[i].position, smoothing * Time.deltaTime);
		}

		// set the previousCamPos to the camera's position at the end of the frame
		previousCamPos = cam.position;
	}

}
