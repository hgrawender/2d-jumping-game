﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour {

    //mój własny skrypt :3
    GameObject player;
    GameObject bf1;
    GameObject bf2;
    Vector3 Ppos;
    Vector3 bf1Pos;
    Vector3 bf2Pos;
    //Vector3 myVector = new Vector3(0,2,0);

    float bfHeight;




    void Start ()
    {
        player = GameObject.Find("Player");
        bf1 = GameObject.Find("Background_far1");
        bf2 = GameObject.Find("Background_far2");
        bfHeight = bf2.GetComponent<SpriteRenderer>().bounds.size.y;
        
    }

    void Update()
    {

        Ppos = player.transform.position;
        bf1Pos = bf1.transform.position;
        bf2Pos = bf2.transform.position;
        //Debug.Log(Pposition);


        //for creation upwards
        if (Ppos.y - bf1Pos.y > 0.9 * bfHeight)
        {
            bf1.transform.position = bf1Pos + Vector3.up * (2 * bfHeight -10); //-1 so they will overlap
        }

        if (Ppos.y - bf2Pos.y > 0.9 * bfHeight)
        {
            bf2.transform.position = bf2Pos + Vector3.up * (2 * bfHeight -10);
        }


        //for creation downwards
        if (Ppos.y - bf1Pos.y <  -bfHeight)
        {
            bf1.transform.position = bf1Pos + Vector3.down * (2 * bfHeight -10); //-1 so they will overlap
        }

        if (Ppos.y - bf2Pos.y <  -bfHeight)
        {
            bf2.transform.position = bf2Pos + Vector3.down * (2 * bfHeight -10);
        }

            
    }


       
}
