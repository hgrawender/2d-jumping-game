﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {

    Text timer;
	// Use this for initialization
	void Start () {
        timer = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {

        timer.text = "TIME: " +(Time.timeSinceLevelLoad).ToString("0.00");
        timer.fontStyle = FontStyle.Bold;
        timer.color = new Color32(0, 0, 0, 170);
	}
}
