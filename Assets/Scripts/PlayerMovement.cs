﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public CharacterController2D controller;
    public Animator animator;
    private Rigidbody2D m_Rigidbody2D; 

    float horizontalMove = 0f;
    bool jump = false;
    bool ice = false;

    public float runSpeed = 40f;

	// Use this for initialization
	void Start () {
        m_Rigidbody2D = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {

        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
        animator.SetFloat(("Speed"), Mathf.Abs(horizontalMove));


        if (Input.GetButton("Jump") && m_Rigidbody2D.velocity.y <= 2.2f)
        {
            jump = true;
            animator.SetBool("isJumping", true);
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }


    void FixedUpdate ()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, jump, ice);
        jump = false;
        ice = false;
    }

    public void OnLanding()
    {
        animator.SetBool("isJumping", false);
    }

}


