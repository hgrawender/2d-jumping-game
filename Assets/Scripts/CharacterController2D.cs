using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;
using System.Collections.Generic;

public class CharacterController2D : MonoBehaviour
{

    [SerializeField]
    private float m_JumpForce = 80f;							// Amount of force added when the player jumps.
    [SerializeField]
    private float m_MoveSpeed = 10f;							// MoveSpeed 
    [Range(0, .3f)]
    [SerializeField]
    private float m_MovementSmoothing = .05f;	// How much to smooth out the movement
    [SerializeField]
    private bool m_AirControl = false;							// Whether or not a player can steer while jumping;
    [SerializeField]
    private LayerMask m_WhatIsGround;							// A mask determining what is ground to the character
    [SerializeField]
    private Transform m_GroundCheck;							// A position marking where to check if the player is grounded.

    const float k_GroundedRadius = .2f;         // Radius of the overlap circle to determine if grounded
    private bool m_Grounded;                    // Whether or not the player is grounded.
    private bool m_Icy = false;                 // Whether or not the player is on ice.
    private bool m_PowerUp = false;             // Whether or not the player is on cake platform that increases jump power.
    private Rigidbody2D m_Rigidbody2D;
    private bool m_FacingRight = true;  // For determining which way the player is currently facing.
    private Vector3 m_Velocity = Vector3.zero;
    public PhysicsMaterial2D icy_mat;
    public AudioSource jump_sound;                                                //jumping sound
    public AudioSource coin_sound;
    public EdgeCollider2D ec; //for jumping on platform from bottom
    public PlatformEffector2D pf;
    public Animator ani;
    public List<GameObject> Enemies = new List<GameObject>();       // List of all platforms (enemies)
    public List<GameObject> Clouds = new List<GameObject>();       // List of all clouds (front)
    public int maxDistance;                                    // the greatest possile distance between two platforms
    public double probability_1, probability_2, probability_3, probability_4;      // probabilities for different lenghts of platform
    public double probability_grass, probability_snow, probability_cake, probability_cloud1;               // probabilities for the type of platorm
    public List<GameObject> Platforms;


    [Header("Events")]
    [Space]

    public UnityEvent OnLandEvent;

    [System.Serializable]
    public class BoolEvent : UnityEvent<bool> { }

    private void Awake()
    {
        m_Rigidbody2D = GetComponent<Rigidbody2D>();

        if (OnLandEvent == null)
            OnLandEvent = new UnityEvent();

       generatePlatforms();
       generateSideWalls();
       generateFrontClouds();

    }



    private void FixedUpdate()
    {

        bool wasGrounded = m_Grounded;
        m_Grounded = false;
        m_Icy = false;
        m_PowerUp = false;

        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {

                if (colliders[i].gameObject.tag == "Ice")
                {
                    m_Icy = true;
                }
                if (colliders[i].gameObject.tag == "PowerUp")
                {
                    m_PowerUp = true;
                }

                m_Grounded = true;
                if (!wasGrounded)
                    OnLandEvent.Invoke();
            }

        }
        CheckIfAbovePlatform();
    }


    public void Move(float move, bool jump, bool ice)
    {

        if (m_Grounded || m_AirControl)
        {


            if (m_Icy)      //Movement for icy conditions
            {
                Vector3 targetVelocity = new Vector2((float)((m_Rigidbody2D.velocity.x / 1.05) + move * m_MoveSpeed * 0.2), m_Rigidbody2D.velocity.y);
                m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);

            }
            else //Movement for normal conditions
            {
                Vector3 targetVelocity = new Vector2(move * m_MoveSpeed, m_Rigidbody2D.velocity.y);
                m_Rigidbody2D.velocity = Vector3.SmoothDamp(m_Rigidbody2D.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);
            }


            if (move > 0 && !m_FacingRight)
            {
                Flip();
            }
            else if (move < 0 && m_FacingRight)
            {
                Flip();
            }
        }

        if (m_Grounded && jump)
        {
            jump_sound.Play();

            m_Grounded = false;
            m_Rigidbody2D.velocity = new Vector2(move * m_MoveSpeed, 0f);
            if (m_PowerUp)
            {
                m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce * 1.3f));
            }
            else
            {
                m_Rigidbody2D.AddForce(new Vector2(0f, m_JumpForce));
            }
        }
    }


    private void Flip()
    {
        m_FacingRight = !m_FacingRight;

        // Lokalna skala razy -1
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void OnCollisionEnter2D(Collision2D coin)
    {
        if (coin.gameObject.tag == "Coin")
        {
            coin_sound.Play();
            Destroy(coin.gameObject);
            Score.bonus += 3;
        }
        //if (coin.gameObject.tag == "MainCamera")
        //{
        //    coin_sound.Play();
        //    //Destroy(coin.gameObject);
        //    Score.bonus += 100;
        //}
    }


    void CheckIfAbovePlatform()
    {
        int points = 0;
        for (int i = 0; i < Platforms.Count; i++)
        {
            if (Platforms[i].transform.position.y >= gameObject.transform.position.y)
            {
                points = i - 1;
                break;
            }
        }

        if (points > Score.scoreValue)
        {
            Score.scoreValue = points;
        }

    }

    void AddCoin(Vector3 temp)
    {
        GameObject coins = GameObject.Find("Coins");

        Vector3 coinPositionModifier = new Vector3(0, 1.0f, 0);
        Vector3 coinScale = new Vector3(0.6f, 0.6f, 0.6f);

        GameObject coin = new GameObject("Coin");
        coin.transform.position += temp + coinPositionModifier;

        coin.AddComponent<SpriteRenderer>();
        coin.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("image 1");
        coin.transform.localScale = coinScale;
        coin.AddComponent<BoxCollider2D>();
       // coin.GetComponent<BoxCollider2D>().isTrigger = true;

        coin.AddComponent<Animator>();
        coin.transform.SetParent(coins.transform);

        ani = coin.GetComponent<Animator>();
        ani.runtimeAnimatorController = Resources.Load("CoinAnimator") as RuntimeAnimatorController;
        
        coin.tag = "Coin";
    }

    void generatePlatforms()
    {
        string[] grounds = { "ground_grass_4", "ground_grass_3", "ground_grass_2", "ground_grass_1",
                             "ground_snow_4", "ground_snow_3", "ground_snow_2", "ground_snow_1",
                             "ground_cake_4", "ground_cake_3", "ground_cake_2", "ground_cake_1"};
        probability_grass = 0.5;
        probability_snow = 0.35;
        probability_cake = 1 - probability_grass - probability_snow;

        int nextCoinIndex = 0;
        int firstCoinIndex = UnityEngine.Random.Range(1, 5);

        GameObject platforms = GameObject.Find("Platforms");

        for (int i = 0; i < 500; i++)
        {
            double distance = 0;
            GameObject objToSpawn = new GameObject("Item" + i);
            string[] groundsOfType = new string[4];
            string randomGround;
            int index;

            //Sprite newPlatform = Resources.Load("E:\unity\2D Jumping Game\Assets\Sprites\Kenny\Environment", typeof(Sprite)) as Sprite;

            objToSpawn.AddComponent<SpriteRenderer>();
            objToSpawn.GetComponent<SpriteRenderer>().sortingLayerName = "Platforms";
            objToSpawn.name = "Item" + i;


            if (i > 0 && (i % 50 != 0))
            {
                // Losowanie randomowego rodzaju platformy: grass, snow lub cake

                double randomGroundType = UnityEngine.Random.Range(0f, 1f);
                if (randomGroundType < probability_grass)
                {
                    Array.Copy(grounds, 0, groundsOfType, 0, 4);
                }
                else if (randomGroundType < probability_grass + probability_snow)
                {
                    Array.Copy(grounds, 4, groundsOfType, 0, 4);
                }
                else
                {
                    Array.Copy(grounds, 8, groundsOfType, 0, 4);
                }

                // Ustalanie prawdopodobie�stwa wyst�pienia platformy o danej szeroko�ci wzgl�dem uzykanego poziomu
                if (i < 20)
                {
                    probability_4 = 0.4; probability_3 = 0.3; probability_2 = 0.2; probability_1 = 0.1;
                }
                else if (i >= 20 && i < 40)
                {
                    probability_4 = 0.3; probability_3 = 0.3; probability_2 = 0.3; probability_1 = 0.2;
                }
                else if (i < 70)
                {
                    probability_4 = 0.1; probability_3 = 0.2; probability_2 = 0.4; probability_1 = 0.2;
                }
                else if (i < 100)
                {
                    probability_4 = 0; probability_3 = 0.1; probability_2 = 0.45; probability_1 = 0.45;
                }
                else
                {
                    probability_4 = 0; probability_3 = 0; probability_2 = 0.3; probability_1 = 0.7;
                }

                // Losowanie randomowego wymiaru platformy

                double randomGroundSize = UnityEngine.Random.Range(0f, 1f);
                if (randomGroundSize < probability_4)
                {
                    index = 0;
                }
                else if (randomGroundSize < probability_4 + probability_3)
                {
                    index = 1;
                }
                else if (randomGroundSize < probability_4 + probability_3 + probability_2)
                {
                    index = 2;
                }
                else
                {
                    index = 3;
                }

                randomGround = groundsOfType[index];

                GameObject previousObject = Enemies[i - 1];
                Vector3 temp = new Vector3(UnityEngine.Random.Range(-7f, 7f), previousObject.transform.position.y + UnityEngine.Random.Range(2.3f, 4.5f), 0);
                objToSpawn.transform.position += temp;              //lub ca�� ("..\\Sprites\\Kenny\\Environment\\ground_wood");
                objToSpawn.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(randomGround);  //defaultowo bierze z folderu Reasources


                float currentObjectWidth = objToSpawn.GetComponent<SpriteRenderer>().bounds.size.x;
                float previousObjectWidth = Enemies[i - 1].GetComponent<SpriteRenderer>().bounds.size.x;
                if (previousObject.GetComponent<SpriteRenderer>().sprite.name.Contains("1"))
                {
                    maxDistance = 7;
                }
                else if (previousObject.GetComponent<SpriteRenderer>().sprite.name.Contains("2") && (objToSpawn.GetComponent<SpriteRenderer>().sprite.name.Contains("1") || objToSpawn.GetComponent<SpriteRenderer>().sprite.name.Contains("2")))
                {
                    maxDistance = 8;
                }
                else
                {
                    maxDistance = 14;
                }

                if (previousObject.transform.position.x < 0 && objToSpawn.transform.position.x > 0)
                {
                    distance = Mathf.Sqrt(
                                    Mathf.Pow(Mathf.Abs((previousObject.transform.position.x + previousObjectWidth) - objToSpawn.transform.position.x), 2)
                                    + Mathf.Pow(objToSpawn.transform.position.y - previousObject.transform.position.y, 2));

                }
                else if (previousObject.transform.position.x > 0 && objToSpawn.transform.position.x < 0)
                {
                    distance = Mathf.Sqrt(
                                    Mathf.Pow(Mathf.Abs((objToSpawn.transform.position.x + currentObjectWidth) - previousObject.transform.position.x), 2)
                                    + Mathf.Pow(previousObject.transform.position.y - objToSpawn.transform.position.y, 2));
                }

                if (distance > maxDistance)
                {
                    Destroy(objToSpawn);
                    //Debug.Log("DISTANCE WAS TOO LONG");
                    i -= 1;
                    continue;
                }

                if (randomGround.StartsWith("ground_snow"))
                {
                    objToSpawn.tag = "Ice";
                    objToSpawn.transform.Rotate(Vector3.forward * UnityEngine.Random.Range(-10f, 10f));
                }
                else if (randomGround.StartsWith("ground_cake"))
                {
                    objToSpawn.tag = "PowerUp";
                }

                if (i == firstCoinIndex || i == nextCoinIndex)
                {
                    int randomNumber = UnityEngine.Random.Range(4, 10);
                    if ((i + randomNumber) % 50 == 0)
                    {
                        randomNumber -= 1;
                    }
                    AddCoin(temp);
                    nextCoinIndex = i + randomNumber;
                }

            }
            else if (i == 0)
            {
                Vector3 temp = new Vector3(-0.5f, -12, 0);
                objToSpawn.transform.position += temp;              //lub ca�� ("..\\Sprites\\Kenny\\Environment\\ground_wood");
                objToSpawn.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("ground_stone");  //defaultowo bierze z folderu Reasources

            }
            else if (i % 50 == 0)
            {
                GameObject previousObject = Enemies[i - 1];
                Vector3 temp = new Vector3(-0.5f, previousObject.transform.position.y + UnityEngine.Random.Range(1.5f, 5f), 0);
                objToSpawn.transform.position += temp;              //lub ca�� ("..\\Sprites\\Kenny\\Environment\\ground_wood");
                objToSpawn.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("ground_stone");  //defaultowo bierze z folderu Reasources
            }


            objToSpawn.AddComponent<EdgeCollider2D>();
            objToSpawn.AddComponent<PlatformEffector2D>();

            ec = objToSpawn.GetComponent<EdgeCollider2D>();
            pf = objToSpawn.GetComponent<PlatformEffector2D>();
            ec.usedByEffector = true;
            pf.surfaceArc = 165;
            Vector2[] colliderpoints;
            colliderpoints = ec.points;



            colliderpoints[0].y = colliderpoints[0].y + 0.37f;          //Musimi to zmieni� je�li zmienimy wysoko�� sprite'a!
            colliderpoints[1].y = colliderpoints[1].y + 0.37f;
            ec.points = colliderpoints;


            //SpriteRenderer sr = objToSpawn.GetComponent<SpriteRenderer>();  //Dodamy platformy do layer Background
            //sr.sortingLayerName = "Background";
            objToSpawn.transform.SetParent(platforms.transform);
            Enemies.Add(objToSpawn);
            objToSpawn.GetComponent<SpriteRenderer>().sortingLayerName = "Platforms";


        }
        Platforms = new List<GameObject>(Enemies);
    }

    void generateSideWalls()
    {
        GameObject walls = GameObject.Find("Walls");

        //Side Walls
        GameObject wallLeft0 = new GameObject("wallLeft0");
        Vector3 pos0 = new Vector3(-100f, 0, 0);
        wallLeft0.transform.localScale += new Vector3(1.0f, 1.0f, 0);
        wallLeft0.transform.position = pos0;
        wallLeft0.AddComponent<SpriteRenderer>();
        wallLeft0.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("rock_left_wall");
        wallLeft0.transform.SetParent(walls.transform);
        float wallSizeY = wallLeft0.GetComponent<SpriteRenderer>().bounds.size.y;

        //Side Walls
        for (int i = 0; i < 100; i++)
        {
            GameObject wallLeft = new GameObject("wallLeft" + i);
            wallLeft.AddComponent<SpriteRenderer>();
            wallLeft.name = "wallLeft" + i;

            Vector3 posL = new Vector3(-11.5f, wallSizeY * (i - 2), 0);
            wallLeft.transform.localScale += new Vector3(1.0f, 1.0f, 0);
            wallLeft.transform.position = posL;              //lub ca�� ("..\\Sprites\\Kenny\\Environment\\ground_wood");
            wallLeft.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("rock_left_wall");  //defaultowo bierze z folderu Reasources
            wallLeft.AddComponent<BoxCollider2D>();
            wallLeft.transform.SetParent(walls.transform);

            GameObject wallRight = new GameObject("wallRight" + i);
            wallRight.AddComponent<SpriteRenderer>();
            wallRight.name = "wallRight" + i;

            Vector3 posR = new Vector3(10.4f, wallSizeY * (i - 2), 0);
            wallRight.transform.localScale += new Vector3(1.0f, 1.0f, 0);
            wallRight.transform.position = posR;              //lub ca�� ("..\\Sprites\\Kenny\\Environment\\ground_wood");
            wallRight.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("rock_right_wall");  //defaultowo bierze z folderu Reasources
            wallRight.AddComponent<BoxCollider2D>();
            wallRight.transform.SetParent(walls.transform);
            // SpriteRenderer sr = objToSpawn.GetComponent<SpriteRenderer>();  //Dodamy platformy do layer Background
            //sr.sortingLayerName = "Background";
  
        }
    }

    void generateFrontClouds()
    {
        string[] clouds = { "Cloud_1", "Cloud_2"};
        double probability = 0.5;
        string randomCloud = "";
        Vector3 initial = new Vector3(-4f, 0f, 0f);
        GameObject backgroundNear = GameObject.Find("Background_near");

        for (int i = 0; i < 100; i++)
        {


            
            GameObject cloud = new GameObject("Cloud" + i);
            Vector3 cloudScale = new Vector3(UnityEngine.Random.Range(1.2f, 2.5f), UnityEngine.Random.Range(1.0f, 2.3f), 1);
            
            double randomIndex = UnityEngine.Random.Range(0f, 1.0f);
            double randomIndex2 = UnityEngine.Random.Range(0f, 1.0f);
            float xPosition = UnityEngine.Random.Range(-8f, -3f);
            randomCloud = clouds[0];

            if(randomIndex < probability)
            {
                randomCloud = clouds[1];
            }

            if(i > 0)
            {
                if (randomIndex2 < probability)
                {
                    xPosition = Math.Abs(xPosition);
                }
                GameObject previousCloud = Clouds[i - 1];
                Vector3 temp = new Vector3(xPosition, previousCloud.transform.position.y + UnityEngine.Random.Range(8f, 20f), 0);
                cloud.transform.position += temp;
                
            }
            else
            {
                cloud.transform.position += initial;
            }

            cloud.transform.localScale = cloudScale;
            cloud.AddComponent<SpriteRenderer>();
            cloud.GetComponent<SpriteRenderer>().sortingLayerName = "Foreground";
            cloud.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(randomCloud);
            cloud.transform.SetParent(backgroundNear.transform);
            Clouds.Add(cloud);
            
            
        }
    }
}
