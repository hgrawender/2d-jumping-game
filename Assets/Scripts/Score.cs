﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{

    public static int scoreValue = 0;
    public static int bonus = 0;
    Text score;

    // Use this for initialization
    void Start()
    {
        score = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {

        score.text = "SCORE: " + (scoreValue + bonus);
        //score.fontSize = 30;
        score.fontStyle = FontStyle.Bold;
        score.color = new Color32(0, 0, 0, 170);

        //Vector3 position = new Vector3(-100, -190, 0);
        //Quaternion whateverItIs = new Quaternion(0,0,0,0);
        //score.rectTransform.SetPositionAndRotation(position, whateverItIs);

    }
}

