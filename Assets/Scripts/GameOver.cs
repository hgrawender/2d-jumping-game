﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOver : MonoBehaviour {

    public AudioSource gameover_sound;
    public Camera cam;
    public Camera CMCam;
    public Animator animator;
    public GameObject quitButton;
    public GameObject resetButton;
    public Text yourScore;
   


    private void Awake()
    {
         quitButton.SetActive(false);
         resetButton.SetActive(false); 
    }

        private void FixedUpdate()
        {
           Vector3 CamPos =  cam.gameObject.transform.position;
           float CamPosY =  cam.gameObject.transform.position.y;
           Vector3 DZpos = transform.position;
           float DifY = CamPosY - DZpos.y;


            Vector3 DZcorrection = new Vector3(0, DifY - 15f, 0);

            if (DZcorrection.y >0 )        //do this only when greater!
            {
               transform.position = DZpos + DZcorrection;// +DZcorrection;
            }

   
          //  Vector3 moveUp = new Vector3(0, 1f, 0);
           // cam.gameObject.transform.position = CamPos + moveUp;
        }




        void OnCollisionEnter2D(Collision2D other)
        {
            SpriteRenderer GOsprite;
            SpriteRenderer GOBackgroundsprite;
            Vector3 DZpos = transform.position;

            if (other.gameObject.tag == "Player")
            {
                gameover_sound.Play();
                animator.SetBool("Dead", true);
                GameObject GameOver = new GameObject("GameOver");
                

                GameOver.transform.position = DZpos + 15*Vector3.up;
                


                Vector3 GOScale = new Vector3(1.6f, 1.6f, 1.6f);
                GameOver.AddComponent<SpriteRenderer>();
                GOsprite = GameOver.GetComponent<SpriteRenderer>();
                GameOver.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("gameover");
                GOsprite.sortingLayerName = "Foreground";
                GOsprite.sortingOrder = 20;
                GameOver.transform.localScale = GOScale;

                GameObject GameOverBackground = new GameObject("GameOverBackground");
                GameOverBackground.transform.position = DZpos + 13 * Vector3.up;

                GameOverBackground.AddComponent<SpriteRenderer>();
                GOBackgroundsprite = GameOverBackground.GetComponent<SpriteRenderer>();
                GameOverBackground.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("gameoverBackgroundInv");
                GOBackgroundsprite.color = new Color(1f, 1f, 1f, .8f); //transparency
                GOBackgroundsprite.sortingLayerName = "Foreground";
                GOBackgroundsprite.sortingOrder = 10;
                GameOverBackground.transform.localScale = GOScale;


                
                yourScore.text = "YOUR SCORE: " + (Score.scoreValue + Score.bonus);

                //yourScore.fontSize = 30;
//                score.fontStyle = FontStyle.Bold;
                yourScore.color = new Color32(0, 0, 0, 170);
                //GameObject yourScore = GameObject.Find("yourScore");

                quitButton.SetActive(true);
                resetButton.SetActive(true);

                animator.SetBool("Dead", true);
                Time.timeScale = 0;


            }
        }
}
