﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using System;

public class Quit : MonoBehaviour {

    public Button quitButton;

    void Start()
    {
        Button btn = quitButton.GetComponent<Button>();
        btn.onClick.AddListener(quitApplication);
    }

    void quitApplication()
    {
        //UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            quitApplication();
        }
    }
}
